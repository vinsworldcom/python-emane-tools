#!/usr/bin/env python

# Copyright (c) - 2024 The MITRE Corporation. All Rights Reserved.

#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions
#  are met:

#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
#  * Neither the name of The MITRE Corporation nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.

#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
#  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
#  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
#  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
#  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
#  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
#  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
#  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
#  POSSIBILITY OF SUCH DAMAGE.

# This is part of a collection of software repositories that aid in the
# creation, management and execution of emulation environments.
# Originally developed for a specific project, we have extracted the
# public components into a "first-stage" build for open access to all
# emulation engineers. Many other internal projects have adopted this
# framework and now with public release, it is freely available to all
# external collaborators and those interested in using emulation
# testbeds.  We welcome any public contributions to make the underlying
# framework better and / or more robust.

# Public release case number:  24-1190

import unittest

import os
import sys

sys.path.append(os.path.join(os.path.dirname(__file__), ".."))

import emanetools.ConfigParse as ConfigParse

def parseLinks(links):
    """Parse links from command line into config file format"""

    linkArray = []
    for link in links.split(","):
        linkArray.append(link)

    return(linkArray)


class Test_PythonTools(unittest.TestCase):
    def setUp(self):
        pass

    def test_linksOverride(self):
        class args(object):
            def __init__(self):
                self.config = "elaps.json.example"
                self.links = ""
        args = args()
        data = ConfigParse.ConfigParse(args.config)
        self.assertEqual(data.linkmap()['1'], '2')
        self.assertEqual(data.linkmap()['2'], '1')
        self.assertEqual(data.linkmap()['3'], '5')
        self.assertEqual(data.linkmap()['4'], '6')
        self.assertEqual(data.linkmap()['5'], '3')
        self.assertEqual(data.linkmap()['6'], '4')
        data.links(parseLinks('1-2,3-4,5-6'))
        self.assertEqual(data.linkmap()['1'], '2')
        self.assertEqual(data.linkmap()['2'], '1')
        self.assertEqual(data.linkmap()['3'], '4')
        self.assertEqual(data.linkmap()['4'], '3')
        self.assertEqual(data.linkmap()['5'], '6')
        self.assertEqual(data.linkmap()['6'], '5')


    def test_linksForce(self):
        class args(object):
            def __init__(self):
                self.config = "elaps.json.example"
                self.links = ""
        args = args()
        data = ConfigParse.ConfigParse(args.config, True)
        data.links(parseLinks('1-2,1-4,1-6'))
        self.assertEqual(data.links()[0], '1-2')
        self.assertEqual(data.links()[1], '1-4')
        self.assertEqual(data.links()[2], '1-6')


if __name__ == '__main__':
    unittest.main()
