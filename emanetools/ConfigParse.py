#!python3

# Copyright (c) - 2024 The MITRE Corporation. All Rights Reserved.

#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions
#  are met:

#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
#  * Neither the name of The MITRE Corporation nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.

#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
#  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
#  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
#  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
#  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
#  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
#  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
#  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
#  POSSIBILITY OF SUCH DAMAGE.

# This is part of a collection of software repositories that aid in the
# creation, management and execution of emulation environments.
# Originally developed for a specific project, we have extracted the
# public components into a "first-stage" build for open access to all
# emulation engineers. Many other internal projects have adopted this
# framework and now with public release, it is freely available to all
# external collaborators and those interested in using emulation
# testbeds.  We welcome any public contributions to make the underlying
# framework better and / or more robust.

# Public release case number:  24-1190

"""
Parsing
"""

__author__ = 'Michael Vincent <mvincent@mitre.org>'
__date__   = 'Wednesday March 03, 2021 05:15:03 PM Eastern Standard Time'

import json


class ConfigParse(object):
    """A JSON configuration parser"""

    def __init__(self, config, force=False):
        """Parse config file"""
        self.configFile = config

        data={}
        with open(config) as config_data:
            data = json.load(config_data)

        if 'linksNoCheck' not in data:
            data['linksNoCheck'] = False

        if force:
            data['linksNoCheck'] = True

        data['linkmap'] = self.__linkmap(data['links'], data['linksNoCheck'])
        self.config = data

    def nems(self):
        """Return list of NEMs"""

        nems = []
        for nem in self.config['nems']:
            nems.append(nem)

        return (nems)

    def links(self, links=[]):
        """Returns or sets the links"""

        if links:
            self.config['linkmap'] = self.__linkmap(links, self.config['linksNoCheck'])
            self.config['links'] = links
        else:
            return (self.config['links'])

    def models(self):
        """Returns the models"""

        return (self.config['models'])

    def linkmap(self):
        """Returns the linkmap"""

        return (self.config['linkmap'])

    def host2nem(self, host):
        """Return list of NEMs for provided host"""

        nems = []
        for nem in self.config['nems']:
            if self.config['nems'][nem]['hostname'] == host:
                nems.append(nem)

        return (nems)

    def nem2host(self, nem):
        """Return the hostname for provided NEM"""

        host = ''
        nem = str(nem)
        if nem in self.config['nems']:
            host = self.config['nems'][nem]['hostname']

        return (host)

    def nem2antenna(self, nem):
        """Return active antenna for provided NEM"""

        ant = 'a'
        nem = str(nem)
        if nem in self.config['nems']:
            if "antenna" in self.config['nems'][nem]:
                ant = self.config['nems'][nem]['antenna']

        return (ant)

    def nem2blockage(self, nem):
        """Return blockage for active antenna for provided NEM"""

        block = 'none'
        ant = self.nem2antenna(nem)
        nem = str(nem)

        if nem in self.config['nems']:
            if ant in self.config['nems'][nem]['antennas']:
                block = self.config['nems'][nem]['antennas'][ant]['blockage']

        return (block)

    def nem2callsign(self, nem):
        """Return callsign for provided NEM"""

        callsign = ''
        nem = str(nem)
        if nem in self.config['nems']:
            if "callsign" in self.config['nems'][nem]:
                callsign = self.config['nems'][nem]['callsign']
            else:
                callsign = self.config['nems'][nem]['hostname']

        return (callsign)

    def nem2type(self, nem):
        """Return the type for provided NEM"""

        type = ''
        nem = str(nem)
        if nem in self.config['nems']:
            if "type" in self.config['nems'][nem]:
                type = self.config['nems'][nem]['type']

        return (type)

    def __linkmap(self, links, force):
        """Create link mapping"""

        linkmap = {}
        for link in links:
            i, j = link.split('-')
            if not force:
                if i in linkmap:
                    print(
                        "error in links: NEM %s already linked to %s, trying to re-link to %s"
                        % (i, linkmap[i], j))
                    exit(1)
            linkmap[i] = j
            if not force:
                if j in linkmap:
                    print(
                        "error in links: NEM %s already linked to %s, trying to re-link to %s"
                        % (j, linkmap[j], i))
                    exit(1)
            linkmap[j] = i

        return (linkmap)
