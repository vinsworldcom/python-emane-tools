#!python3

# Copyright (c) - 2024 The MITRE Corporation. All Rights Reserved.

#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions
#  are met:

#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
#  * Neither the name of The MITRE Corporation nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.

#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
#  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
#  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
#  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
#  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
#  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
#  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
#  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
#  POSSIBILITY OF SUCH DAMAGE.

# This is part of a collection of software repositories that aid in the
# creation, management and execution of emulation environments.
# Originally developed for a specific project, we have extracted the
# public components into a "first-stage" build for open access to all
# emulation engineers. Many other internal projects have adopted this
# framework and now with public release, it is freely available to all
# external collaborators and those interested in using emulation
# testbeds.  We welcome any public contributions to make the underlying
# framework better and / or more robust.

# Public release case number:  24-1190

"""
Routines for calculating and converting coordinate space.
https://github.com/adjacentlink/emane/wiki/Computing-Antenna-Gain
"""

__author__ = 'Michael Vincent <mvincent@mitre.org>'
__date__   = 'Wednesday March 03, 2021 05:15:03 PM Eastern Standard Time'

import math

EARTH_RADIUS = 6371009  # meters

class Polar(object):
    """
    Polar coordinates are:
      - **location**:  latitude, longitude, altitude
      - **orientation**: roll, pitch, yaw
      - **velocity**: azimuth, elevation, magnitude
    """

    def __init__(
        self,
        latitude=0,
        longitude=0,
        altitude=0,
        pitch=0,
        roll=0,
        yaw=0,
        azimuth=0,
        elevation=0,
        magnitude=0
    ):
        """
        Args:
            latitude (float): Latitude decimal degrees
            longitude (float): Longitude decimal degrees
            altitude (float): Altitude meters
            pitch (float): Pitch degrees of 360
            roll (float): Roll degrees of 360
            yaw (float): Yaw degrees of 360
            azimuth (float): Azimuth degrees of 360
            elevation (float): Elevation degrees of -90 to 90
            magnitude (float): Magnitude meters/second
        """
        self.latitude  = latitude
        self.longitude = longitude
        self.altitude  = altitude
        self.pitch     = pitch
        self.roll      = roll
        self.yaw       = yaw
        self.azimuth   = azimuth
        self.elevation = elevation
        self.magnitude = magnitude


class Sphere(object):
    """
    Sphere coordinates are:
      - azimuth
      - elevation
      - distance

    Sphere object defines the angles to connect two points based on their polar
    coordinates.  As a by-product, the distance (i.e., range) (in meters) is
    also provided.
    """
    def __init__(self, azimuth=0, elevation=0, distance=0):
        """
        Args:
            azimuth (float): Azimuth degrees of 360
            elevation (float): Elevation degrees -90 to 90
            distance (float): Distance meters
        """
        self.azimuth = azimuth
        self.elevation = elevation
        self.distance = distance

    def calculate(self, node0, node1, a21_fix=False):
        """
        Takes two sets of polar coordinates and returns the set of
        spherical coordinates for the first to point towards the second.
        For the "reverse" angles, call this again with the two sets of
        polar coordinates reversed.

        Args:
            node0 (object): ``Polar`` object representing node0
            node1 (object): ``Polar`` object representing node1
            a21_fix (bool): Use the fix in EMANE v1.4.1 (commit 509e7c6) and newer

        Returns the antenna pointing for node[RX]

        Example::

            forward = Sphere()
            forward.calculate(nodeRx,nodeTx)
            print(forward.azimuth, forward.elevation)
        """
        self.a21_fix = a21_fix

        if ( node0.latitude == node1.latitude and
             node0.longitude == node1.longitude and
             node0.altitude == node1.altitude ):
            return

        #Define the WGS84 ellipsoid constants
        SEMI_MAJOR = 6378137
        SEMI_MINOR = 6356752.3142

        f = 1 / 298.257223563
        e2 = 2 * f - f**2

        # Convert to radians
        tlatitude = math.radians(node1.latitude)
        rlatitude = math.radians(node0.latitude)
        tlongitude = math.radians(node1.longitude)
        rlongitude = math.radians(node0.longitude)
        taltitude = node1.altitude
        raltitude = node0.altitude
        tpitch = math.radians(node1.pitch)
        rpitch = math.radians(node0.pitch)
        troll = math.radians(node1.roll)
        rroll = math.radians(node0.roll)
        tyaw = math.radians(node1.yaw)
        ryaw = math.radians(node0.yaw)
        tazimuth = math.radians(node1.azimuth)
        razimuth = math.radians(node0.azimuth)
        televation = math.radians(node1.elevation)
        relevation = math.radians(node0.elevation)

        #Compute the prime vertical radius of curvature
        Nt = SEMI_MAJOR / math.sqrt(1 - e2 * math.sin(tlatitude)**2)
        Nr = SEMI_MAJOR / math.sqrt(1 - e2 * math.sin(rlatitude)**2)

        #Perform the following coordinate transformation for both
        # the transmitter and receiver
        #[Xt, Yt, Ut] = Transmitter Position ECEF
        Xt = (Nt + taltitude) * math.cos(tlatitude) * math.cos(tlongitude)
        Yt = (Nt + taltitude) * math.cos(tlatitude) * math.sin(tlongitude)
        Ut = (Nt * (1 - e2) + taltitude) * math.sin(tlatitude)

        #[Xr, Yr, Ur] = Receiver Position ECEF
        Xr = (Nr + raltitude) * math.cos(rlatitude) * math.cos(rlongitude)
        Yr = (Nr + raltitude) * math.cos(rlatitude) * math.sin(rlongitude)
        Ur = (Nr * (1 - e2) + raltitude) * math.sin(rlatitude)

        #Compute the ECEF vector between receiver and transmitter
        X = Xt - Xr
        Y = Yt - Yr
        Z = Ut - Ur

        # 2) Transform ECEF to NEU (North, East Up) in receiving NEM's frame

        N1 = -X * math.sin(rlatitude) * math.cos(rlongitude) - Y * math.sin(
            rlatitude) * math.sin(rlongitude) + Z * math.cos(rlatitude)
        E1 = -X * math.sin(rlongitude) + Y * math.cos(rlongitude)
        U1 = X * math.cos(rlatitude) * math.cos(rlongitude) + Y * math.cos(
            rlatitude) * math.sin(rlongitude) + Z * math.sin(rlatitude)

        # 3) Adjust yaw and pitch angles to account for velocity vector

        tyaw += tazimuth
        tpitch += televation

        ryaw += razimuth
        rpitch += relevation

        # 4) Perform the following transformation to account for yaw, pitch
        #    and roll

        #Define the transformation matrix
        step4 = self.__step4(rroll, rpitch, ryaw)

        N = N1 * step4['a11'] + E1 * step4['a12'] + U1 * step4['a13']
        E = N1 * step4['a21'] + E1 * step4['a22'] + U1 * step4['a23']
        U = N1 * step4['a31'] + E1 * step4['a32'] + U1 * step4['a33']

        # 5) Update NEU to include receiving node's location on the platform

        # N E U from antenna profile manifest of receiver
        Napmr = 0
        Eapmr = 0
        Uapmr = 0

        # N E U from antenna profile manifest of transmitter
        Napmt = 0
        Eapmt = 0
        Uapmt = 0

        N += Napmr
        E += Eapmr
        U += Uapmr

        # 6) Rotate transmitter's antenna placement into receiver's coordinate
        #    frame using the same transformation defined in Step 4 above
        #    replacingN1, E1 and U1 with the transmitter's antenna placement
        #    valuesobtained from the antenna profile manifest.

        step4 = self.__step4( troll, tpitch, tyaw )

        N += Napmt * step4['a11'] + Eapmt * step4['a12'] + Uapmt * step4['a13']
        E += Napmt * step4['a21'] + Eapmt * step4['a22'] + Uapmt * step4['a23']
        U += Napmt * step4['a31'] + Eapmt * step4['a32'] + Uapmt * step4['a33']

        # 7) Compute range from receiver to transmitter

        range = math.sqrt( N**2 + E**2 + U**2 )

        # 8) Compute bearing and elevation from receiver to transmitter

        # No work - use the other method below
        # my bearing   = atan( E / N )
        theta     = math.degrees(math.atan2( N, E ))
        bearing   = 90.0 - theta
        elevation = math.degrees(math.asin( U / range ))

        if bearing < 0.0:
            bearing += 360.0
        if bearing > 360.0:
            bearing -= 360.0

        self.azimuth = bearing
        self.elevation = elevation
        self.distance = range


    def __step4(self, roll, pitch, yaw):
        matrix = {}
        matrix['a11']     =  math.cos(pitch) * math.cos(yaw)
        matrix['a12']     =  math.cos(pitch) * math.sin(yaw)
        matrix['a13']     =  math.sin(pitch)
        if self.a21_fix:
            # correctd fix EMANE v1.4.1 and newer
            matrix['a21'] =  math.sin(pitch) * math.cos(yaw)   * math.sin(roll)  - math.cos(roll) * math.sin(yaw)
        else:
            # original upto EMANE v1.3.3
            matrix['a21'] = -math.sin(pitch) * math.cos(yaw)   * math.sin(roll)  - math.cos(roll) * math.sin(yaw)
        matrix['a22']     =  math.cos(roll)  * math.cos(yaw)   + math.sin(pitch) * math.sin(roll) * math.sin(yaw)
        matrix['a23']     = -math.cos(pitch) * math.sin(roll)
        matrix['a31']     = -math.cos(yaw)   * math.sin(pitch) * math.cos(roll)  - math.sin(yaw)  * math.sin(roll)
        matrix['a32']     = -math.sin(yaw)   * math.sin(pitch) * math.cos(roll)  + math.sin(roll) * math.cos(yaw)
        matrix['a33']     =  math.cos(pitch) * math.cos(roll)
        return matrix


def target(slat=0, slon=0, salt=0, az=0, el=0, dist=0):
    """
    Calculate target LLA given origin LLA, azimuth, elevation, distance.

    Args:
        Coords.Polar (object): source point
        Coords.Sphere (object): direction and range to target

    or

    Args:
        slat (float): source Latitude decimal degrees
        slon (float): source Longitude decimal degrees
        salt (float): source Altitude meters
        az (float): target Azimuth degrees of 360
        el (float): target Elevation degrees -90 to 90
        dist (float): target Distance meters
    """
    if isinstance(slat, Polar) and isinstance(slon, Sphere):
        az   = slon.azimuth
        el   = slon.elevation
        dist = slon.distance

        salt = slat.altitude
        slon = slat.longitude
        slat = slat.latitude

    slat = math.radians(slat)
    slon = math.radians(slon)
    az   = math.radians(az)
    el   = math.radians(el)

    dlat = math.asin( math.sin(slat) * math.cos(dist/EARTH_RADIUS) + math.cos(slat) * math.sin(dist/EARTH_RADIUS) * math.cos(az) )
    dlon = slon + math.atan2( math.sin(az) * math.sin(dist/EARTH_RADIUS) * math.cos(slat), math.cos(dist/EARTH_RADIUS) - math.sin(slat) * math.sin(dlat) )

    dlat = math.degrees(dlat)
    dlon = math.degrees(dlon)

    # calculate altitude (https://www.mathsisfun.com/algebra/trig-solving-sas-triangles.html)
    dalt = math.sqrt(dist**2 + (salt+EARTH_RADIUS)**2 - 2 * dist * (salt+EARTH_RADIUS) * math.cos(math.radians(90) + el)) - EARTH_RADIUS

    return (dlat, dlon, dalt)


def midpoint(slat=0, slon=0, salt=0, dlat=0, dlon=0, dalt=0):
    """
    Calculate midpoint LLA given origin LLA and destination LLA.

    Args:
        Coords.Polar (object): source point
        Coords.Polar (object): destination point

    or

    Args:
        slat (float): source Latitude decimal degrees
        slon (float): source Longitude decimal degrees
        salt (float): source Altitude meters
        dlat (float): destination Latitude decimal degrees
        dlon (float): destination Longitude decimal degrees
        dalt (float): destination Altitude meters
    """
    if isinstance(slat, Polar) and isinstance(slon, Polar):
        dalt = slon.altitude
        dlon = slon.longitude
        dlat = slon.latitude

        salt = slat.altitude
        slon = slat.longitude
        slat = slat.latitude

    slat = math.radians(slat)
    slon = math.radians(slon)
    dlat = math.radians(dlat)
    dlon = math.radians(dlon)

    Bx = math.cos(dlat) * math.cos(dlon-slon)
    By = math.cos(dlat) * math.sin(dlon-slon)

    mlat = math.atan2(math.sin(slat) + math.sin(dlat), math.sqrt( (math.cos(slat)+Bx)**2 + By**2 ) )
    mlon = slon + math.atan2(By, math.cos(slat) + Bx)

    mlat = math.degrees(mlat)
    mlon = math.degrees(mlon)

    malt = abs(salt - dalt) / 2 + min(salt, dalt)

    return (mlat, mlon, malt)

    ##########

if __name__ == '__main__':

    coords = [
        {
            "slla" : (40.0, -73.0, 3000.0),
            "dlla" : (40.0, -72.0, 3.0)
        },
        {
            "slla" : (40.025495, -74.315441, 3.0),
            "dlla" : (40.025495, -74.312501, 3.0)
        },
        {
            "slla" : (35.67883, -117.6765, 682.0),
            "dlla" : (35.516579, -117.1115, 1000.0)
        }
    ]

    for coord in coords:
        srclat   = coord['slla'][0]
        srclon   = coord['slla'][1]
        srcalt   = coord['slla'][2]
        src_node = Polar()
        src_node.latitude, src_node.longitude, src_node.altitude = coord['slla']

        dstlat   = coord['dlla'][0]
        dstlon   = coord['dlla'][1]
        dstalt   = coord['dlla'][2]
        dst_node = Polar()
        dst_node.latitude, dst_node.longitude, dst_node.altitude = coord['dlla']

        forward = Sphere()
        reverse = Sphere()
        forward.calculate(src_node, dst_node)
        reverse.calculate(dst_node, src_node)

        print("source        :", coord['slla'])
        print("destination   :", coord['dlla'])

        print(f"forward       : ({forward.azimuth}, {forward.elevation}, {forward.distance})")
        print(f"reverse       : ({reverse.azimuth}, {reverse.elevation}, {reverse.distance})")

        tgtaz   = forward.azimuth
        tgtel   = forward.elevation
        tgtdist = forward.distance

        print("target(crds)  :", target(srclat, srclon, srcalt, tgtaz, tgtel, tgtdist))
        print("target(obj)   :", target(src_node, forward))

        print("midpoint(crds):", midpoint(srclat, srclon, srcalt, dstlat, dstlon, dstalt))
        print("midpoint(obj) :", midpoint(src_node, dst_node))

        print("----")

    print("Check DivBy0  :", target())
    print("Check DivBy0  :", midpoint())
