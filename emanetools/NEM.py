#!python3

# Copyright (c) - 2024 The MITRE Corporation. All Rights Reserved.

#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions
#  are met:

#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
#  * Neither the name of The MITRE Corporation nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.

#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
#  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
#  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
#  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
#  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
#  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
#  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
#  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
#  POSSIBILITY OF SUCH DAMAGE.

# This is part of a collection of software repositories that aid in the
# creation, management and execution of emulation environments.
# Originally developed for a specific project, we have extracted the
# public components into a "first-stage" build for open access to all
# emulation engineers. Many other internal projects have adopted this
# framework and now with public release, it is freely available to all
# external collaborators and those interested in using emulation
# testbeds.  We welcome any public contributions to make the underlying
# framework better and / or more robust.

# Public release case number:  24-1190

"""
A NEM object that has the NEM number and its polar coordinates.
"""

__author__ = 'Michael Vincent <mvincent@mitre.org>'
__date__   = 'Wednesday March 03, 2021 05:15:03 PM Eastern Standard Time'

import sys
import socket
import emanetools.Coords as Coords
from emanetools.helpers import transaction,getManifest
try:
    import emane.shell.remotecontrolportapi_pb2 as remotecontrolportapi_pb2
except:
    import emanesh.remotecontrolportapi_pb2 as remotecontrolportapi_pb2

PORT = 47000

class NEM(object):
    """
    A NEM object that has the NEM number and its polar coordinates.
    """

    def __init__(self, nem = 0, coords = 0):
        """
        :param nem: the NEM number
        :param coords: the ``Polar`` coordinates object for the NEM
        """
        self.nem = nem
        self.coords = Coords.Polar()


def getNems(node, port = PORT):
    """
    Given an emane node, return all NEMs in that node only by examining the
    PHY layer LocationEventInfoTable table in EMANE shell.  NEM objects are
    returned as dictionary values with NEM number as the key.
    """

    try:
        manifest, response = __getManifest(node, port)
    except:
        raise

    if len(response.query.statisticTable.tables[0].rows) == 0:
        raise Exception("No response")

    localNems = {}
    for nem in manifest:
        localNems[str(nem)] = 1

    nems = {}
    for row in response.query.statisticTable.tables[0].rows:
        nemId = str(row.values[0].u32Value)
        if nemId in localNems:
            nems[nemId] = NEM()
            nems[nemId].nem = nemId
            nems[nemId].coords.latitude = row.values[1].dValue
            nems[nemId].coords.longitude = row.values[2].dValue
            nems[nemId].coords.altitude = row.values[3].dValue
            nems[nemId].coords.pitch = row.values[4].dValue
            nems[nemId].coords.roll = row.values[5].dValue
            nems[nemId].coords.yaw = row.values[6].dValue
            nems[nemId].coords.azimuth = row.values[7].dValue
            nems[nemId].coords.elevation = row.values[8].dValue
            nems[nemId].coords.magnitude = row.values[9].dValue

    return nems


def getNemsAll(node, port = PORT):
    """
    Given an emane node, return all NEMs in the simulation by examining the
    PHY layer LocationEventInfoTable table in EMANE shell.  NEM objects are
    returned as dictionary values with NEM number as the key.
    """

    try:
        manifest, response = __getManifest(node, port)
    except:
        raise

    if len(response.query.statisticTable.tables[0].rows) == 0:
        raise Exception("No data in response")

    nems = {}
    for row in response.query.statisticTable.tables[0].rows:
        nemId = str(row.values[0].u32Value)
        nems[nemId] = NEM()
        nems[nemId].nem = nemId
        nems[nemId].coords.latitude = row.values[1].dValue
        nems[nemId].coords.longitude = row.values[2].dValue
        nems[nemId].coords.altitude = row.values[3].dValue
        nems[nemId].coords.pitch = row.values[4].dValue
        nems[nemId].coords.roll = row.values[5].dValue
        nems[nemId].coords.yaw = row.values[6].dValue
        nems[nemId].coords.azimuth = row.values[7].dValue
        nems[nemId].coords.elevation = row.values[8].dValue
        nems[nemId].coords.magnitude = row.values[9].dValue

    return nems


def __getManifest(node, port):

    sock = socket.socket()
    sock.settimeout(5)
    try:
        sock.connect((node,port))
    except:
        raise Exception("Error: %s" % sys.exc_info()[0])

    sequence = 1
    manifest = getManifest(sock,sequence)
    for layer in manifest[min(manifest)]:
        if layer.type == "PHY":
            buildId = layer.buildId

    sequence += 1
    request = remotecontrolportapi_pb2.Request()
    request.sequence = sequence
    request.type = remotecontrolportapi_pb2.Request.TYPE_REQUEST_QUERY
    request.query.type = remotecontrolportapi_pb2.TYPE_QUERY_STATISTICTABLE
    request.query.statisticTable.buildId = buildId
    request.query.statisticTable.names.append('LocationEventInfoTable')

    response = transaction(sock,request)
    sock.close()

    return manifest, response
