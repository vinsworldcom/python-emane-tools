#!python

# Copyright (c) - 2024 The MITRE Corporation. All Rights Reserved.

#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions
#  are met:

#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
#  * Neither the name of The MITRE Corporation nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.

#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
#  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
#  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
#  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
#  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
#  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
#  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
#  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
#  POSSIBILITY OF SUCH DAMAGE.

# This is part of a collection of software repositories that aid in the
# creation, management and execution of emulation environments.
# Originally developed for a specific project, we have extracted the
# public components into a "first-stage" build for open access to all
# emulation engineers. Many other internal projects have adopted this
# framework and now with public release, it is freely available to all
# external collaborators and those interested in using emulation
# testbeds.  We welcome any public contributions to make the underlying
# framework better and / or more robust.

# Public release case number:  24-1190

"""
Antenna module for pointing antennas.  This is a subclass of
Coords.Sphere which is the azimuth and elevation angles to point.
"""

__author__ = 'Michael Vincent <mvincent@mitre.org>'
__date__   = 'Wednesday March 03, 2021 05:15:03 PM Eastern Standard Time'

from emanetools.Coords import Sphere
from emane.events import EventService
from emane.events import AntennaProfileEvent

GROUP  = "224.1.2.8"
PORT   = 45703
DEVICE = "emanenode0"

class Event(Sphere):
    """An antenna event.  Points antenna using the EMANE API."""
    def __init__(self, azimuth=0, elevation=0, distance=0):
        """A subclass of ``Sphere``"""
        super(Event, self).__init__(azimuth, elevation, distance)

    def pointAntenna(self, node, nem, antenna = 0):
        """
        Points antenna of the given hostname, NEM and antenna.

        :param node: hostname
        :param nem: NEM number
        :param antenna: antenna ID
        """

        nem = int(nem)

        service = EventService((GROUP,PORT,DEVICE))
        event   = AntennaProfileEvent()
        targets = [0]

        kwargs              = {}
        kwargs['profile']   = int(antenna)
        kwargs['azimuth']   = float(self.azimuth)
        kwargs['elevation'] = float(self.elevation)

        for i in targets:
            try:
                event.append(nem,**kwargs);
            except Exception as exp:
                print(exp)

            service.publish(i,event)
