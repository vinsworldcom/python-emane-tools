# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

import os
import sys
import re
sys.path.insert(0, os.path.abspath('../..'))

project = 'Python EMANE Tools'
copyright = '2024, The MITRE Corporation.  Public release case number: 24-1190'

setup_file_path = os.path.join(os.path.abspath(os.path.dirname(__file__)), r'../../setup.py')
setup_file = open(setup_file_path)
setup_contents = setup_file.read()
setup_file.close()

match = re.search(r'\s+author\s*\=\s*[\'\"]([A-Za-z\s\,\.]+)[\'\"]', setup_contents)
if match:
	author = match.group(1)
else:
	raise RuntimeError("Unknown author")

match = re.search(r'\s+version\s*\=\s*[\'\"]([0-9.]+)[\'\"]', setup_contents)
if match:
	version = match.group(1)
	release = match.group(1)
else:
	raise RuntimeError("Unknown version")

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = ['sphinx.ext.autodoc', 'sphinx.ext.napoleon']

templates_path = ['_templates']
exclude_patterns = []
autoclass_content = 'both'


# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'classic'
html_static_path = ['_static']
