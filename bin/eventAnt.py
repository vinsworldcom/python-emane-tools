#!/usr/bin/env python3

# Copyright (c) - 2024 The MITRE Corporation. All Rights Reserved.

#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions
#  are met:

#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
#  * Neither the name of The MITRE Corporation nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.

#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
#  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
#  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
#  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
#  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
#  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
#  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
#  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
#  POSSIBILITY OF SUCH DAMAGE.

# This is part of a collection of software repositories that aid in the
# creation, management and execution of emulation environments.
# Originally developed for a specific project, we have extracted the
# public components into a "first-stage" build for open access to all
# emulation engineers. Many other internal projects have adopted this
# framework and now with public release, it is freely available to all
# external collaborators and those interested in using emulation
# testbeds.  We welcome any public contributions to make the underlying
# framework better and / or more robust.

# Public release case number:  24-1190

"""
Point Antenna

Script gets GPS positions from EMANE shell and points antennas
according to input JSON file defining links.
"""

try:
    from importlib import metadata
except ImportError:
    # Running on pre-3.8 Python; use importlib-metadata package
    import importlib_metadata as metadata
__author__  = 'Michael Vincent <mvincent@mitre.org>'
__date__    = 'Wednesday March 03, 2021 05:15:03 PM Eastern Standard Time'
__version__ = metadata.version('emanetools')

import os
import sys
import time
import argparse
import json

import emanetools.Antenna.Event as Antenna
import emanetools.ConfigParse as ConfigParse
import emanetools.NEM as NEM


class _Version(argparse.Action):
    """Print Modules, Python, OS, Program info."""

    def __init__(self, nargs=0, **kw):
        super(_Version, self).__init__(nargs=nargs, **kw)

    def __call__(self, parser, namespace, values, option_string=None):
        print('\nModules, Python, OS, Program info:')
        print('  ' + sys.argv[0])
        print('  Version               ' + __version__)
        print('    argparse            ' + argparse.__version__)
        # Additional modules
        print('    json                ' + json.__version__)
        # Additional modules
        print('    Python version      %s.%s.%s' % sys.version_info[:3])
        print('    Python executable   ' + sys.executable)
        print('    OS                  ' + sys.platform)
        print('\n')
        sys.exit(0)


def parseArgs():
    """Parse command line arguments"""

    parser = argparse.ArgumentParser(description='Usage:')
    parser.add_argument(
        '-C', '--config', type=str,
        help="Input file maps NEMS to nodes, links and antennas.")
    parser.add_argument(
        '-f', '--force', action='store_true',
        help="Allow override basic config checks (overlap links).")
    parser.add_argument(
        '-l', '--links', type=str,
        help="Links override links in config.")
    parser.add_argument(
        '-r', '--repeat', type=int, default=1,
        help="Number of times to call antenna pointing [Default 1].")
    parser.add_argument(
        '-v', '--verbose', action='store_true',
        help="Print verbose status.")
    parser.add_argument(
        '-w', '--wait', type=int, default=1,
        help="Number of seconds to wait between repeats [Default 1].")
    parser.add_argument(
        '-V', '--versions', action=_Version,
        help="Print Modules, Python, OS, Program info.")
    parser.add_argument(
        'argv', nargs='*',  # use '*' for optional
        help="Command line arguments.")
    args = parser.parse_args()

    if not args.config:
        print("Config file required")
        exit(1)

    if args.repeat < 1:
        print("--repeat must be >= 1")
        args.repeat = 1

    return(args)


def parseLinks(links):
    """Parse links from command line into config file format"""

    linkArray = []
    for link in links.split(","):
        linkArray.append(link)

    return(linkArray)


def run(args, data):
    """Run"""

    # Get positions from EMANE SHELL
    while True:
        try:
            nems = NEM.getNemsAll(data.nem2host(min(data.nems())))
            break
        except Exception as error:
            if args.verbose:
                print("%s: %s" % (os.path.basename(__file__), error))
            time.sleep(1)

    for link in data.links():
        i, j = link.split('-')

        forward = Antenna.Event()
        reverse = Antenna.Event()
        forward.calculate(nems[i].coords, nems[j].coords)
        reverse.calculate(nems[j].coords, nems[i].coords)

        if args.verbose:
            print("%s: {\"azimuth\":%s, \"elevation\":%s, \"distance\":%s}" % (nems[i].nem, forward.azimuth, forward.elevation, forward.distance))
            print("%s: {\"azimuth\":%s, \"elevation\":%s, \"distance\":%s}" % (nems[j].nem, reverse.azimuth, reverse.elevation, reverse.distance))

        forward.pointAntenna(data.nem2host(nems[i].nem),
          nems[i].nem, data.nem2antenna(nems[i].nem))
        reverse.pointAntenna(data.nem2host(nems[j].nem),
          nems[j].nem, data.nem2antenna(nems[j].nem))

    return(0)


def main():
    """Main Program."""
    args = parseArgs()
    data = ConfigParse.ConfigParse(args.config, args.force)

    if args.links:
        data.links(parseLinks(args.links))

    ret = 1
    repeat = list(range(0, args.repeat))

    for r in repeat:
        ret = run(args, data)
        if args.repeat > 1 and (r + 1) != args.repeat:
            time.sleep(args.wait)
    return(ret)


if __name__ == '__main__':
    main()
