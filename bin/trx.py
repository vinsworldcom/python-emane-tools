#!/usr/bin/env python3

# Copyright (c) - 2024 The MITRE Corporation. All Rights Reserved.

#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions
#  are met:

#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
#  * Neither the name of The MITRE Corporation nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.

#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
#  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
#  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
#  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
#  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
#  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
#  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
#  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
#  POSSIBILITY OF SUCH DAMAGE.

# This is part of a collection of software repositories that aid in the
# creation, management and execution of emulation environments.
# Originally developed for a specific project, we have extracted the
# public components into a "first-stage" build for open access to all
# emulation engineers. Many other internal projects have adopted this
# framework and now with public release, it is freely available to all
# external collaborators and those interested in using emulation
# testbeds.  We welcome any public contributions to make the underlying
# framework better and / or more robust.

# Public release case number:  24-1190

__author__  = 'Michael Vincent <mvincent@mitre.org>'
__date__    = 'Monday, November 20, 2023 9:44 PM'

import argparse
import json
import math
import sys

# constants
THERMAL_NOISE = -174
FSPL          = 41.916900439033640 # https://github.com/adjacentlink/emane/blob/687b8d08ccc196d8cc61ea10572d0f9a5d42cda7/src/libemane/freespacepropagationmodelalgorithm.h#L50

# defaults
DEF_DISTANCE  = 10000.0
DEF_FREQUENCY = 100000000.0
DEF_TXPOWER   = 20.0

# args
parser = argparse.ArgumentParser(
    formatter_class=argparse.RawDescriptionHelpFormatter,
    description="""
Given some inputs, calculates the outputs.

  {
    "txpower": 20.0,
    "rxpower": -52.44778322188337,
    "rxsensitivity": -110.0,
    "sinr": 57.55221677811663
  }

""")
parser.add_argument("-s", "--systemnoise",
                    action="store",
                    type=float,
                    default=4.0,
                    help="System noise")
parser.add_argument("-b", "--bandwidth",
                    action="store",
                    type=float,
                    default=1000000.0,
                    help="Bandwidth in Hz")
parser.add_argument("-d", "--distance",
                    action="store",
                    type=float,
                    help="Distance in meters")
parser.add_argument("-f", "--frequency",
                    action="store",
                    type=float,
                    help="Tx frequency in Hz")
parser.add_argument("-p", "--pathloss",
                    action="store",
                    type=float,
                    help="Pathloss in dB")
parser.add_argument("-P", "--prettyprint",
                    action="store",
                    type=int,
                    default=0,
                    help="Pretty-print output JSON")
parser.add_argument("-r", "--rxgain",
                    action="store",
                    type=float,
                    default=0.0,
                    help="Rx gain in dB")
parser.add_argument("-R", "--rxpower",
                    action="store",
                    type=float,
                    help="Rx power in dB")
parser.add_argument("-t", "--txgain",
                    action="store",
                    type=float,
                    default=0.0,
                    help="Tx gain in dB")
parser.add_argument("-T", "--txpower",
                    action="store",
                    type=float,
                    help="Tx power in dB")
parser.add_argument("-v", "--verbose",
                    action="store_true",
                    help="Verbose output")
args = parser.parse_args()

# warnings of misuse
if args.txpower and args.rxpower:
    print(f"ignoring Tx power `{args.txpower}'", file=sys.stderr)

# START
if not args.frequency:
    print(f"assuming defalt frequency `{DEF_FREQUENCY}'", file=sys.stderr)
    args.frequency = DEF_FREQUENCY

    ##########

def distance(pathloss, frequency):
    """
    Calculate the distance.  From:
    https://github.com/adjacentlink/emane/blob/687b8d08ccc196d8cc61ea10572d0f9a5d42cda7/src/libemane/freespacepropagationmodelalgorithm.h#L69

    Args:
        pathloss (float): pathloss in dB
        frequency (float): frequency in Hz

    Returns:
        distance (float): distance in meters
    """
    return ((10 ** (pathloss / 20)) / (FSPL * (frequency / 1000000.0))) * 1000

def pathloss(frequency, distance):
    """
    Calculate the pathloss.  From:
    https://github.com/adjacentlink/emane/blob/687b8d08ccc196d8cc61ea10572d0f9a5d42cda7/src/libemane/freespacepropagationmodelalgorithm.h#L69

    Args:
        frequency (float): frequency in Hz
        distance (float): distance in meters

    Returns:
        pathloss (float): pathloss in dB
    """
    return 20 * math.log10(FSPL * (frequency / 1000000.0) * (distance / 1000.0))

def txpower(rxpower, txgain, rxgain, pathloss):
    """
    Calculate the TX power.  From:
    https://github.com/adjacentlink/emane/wiki/Physical-Layer-Model#receive-power-calculation

    Args:
        rxpower (float): RX power in dB
        txgain (float): TX gain in dB
        rxgain (float): RX gain in dB
        pathloss (float): pathloss in dB

    Returns:
        txpower (float): TX power in dB
    """
    return rxpower - txgain - rxgain + pathloss

def rxpower(txpower, txgain, rxgain, pathloss):
    """
    Calculate the RX power.  From:
    https://github.com/adjacentlink/emane/wiki/Physical-Layer-Model#receive-power-calculation

    Args:
        txpower (float): TX power in dB
        txgain (float): TX gain in dB
        rxgain (float): RX gain in dB
        pathloss (float): pathloss in dB

    Returns:
        rxpower (float): RX power in dB
    """
    return txpower + txgain + rxgain - pathloss

def rxsensitivity(systemnoise, bandwidth):
    """
    Return the RX sensitivity.  From:
    https://github.com/adjacentlink/emane/wiki/Physical-Layer-Model#receive-power-calculation

    Args:
        systemnoise (float): system noise in dB
        bandwidth (float): bandwidth in Hz

    Returns:
        rxsensitivity (float) RX sensitivity in dB
    """
    return THERMAL_NOISE + systemnoise + (10 * math.log10(bandwidth))

def sinr(rxpower, rxsensitivity):
    """
    Calculate the Signal to Interference and Noise Ratio (SINR).  From:
    https://github.com/adjacentlink/emane/wiki/Physical-Layer-Model#receive-power-calculation

    Args:
        rxpower (float): RX power in dB
        rxsensitvity (flaot): RX sensitivity in dB
    """
    return rxpower - rxsensitivity

    ##########

if args.pathloss:
    args.distance = distance(args.pathloss, args.frequency)
else:
    if not args.distance:
        print(f"assuming defalt distance `{DEF_DISTANCE}'", file=sys.stderr)
        args.distance = DEF_DISTANCE
    args.pathloss = pathloss(args.frequency, args.distance)

if not args.txpower:
    print(f"assuming defalt Tx power `{DEF_TXPOWER}'", file=sys.stderr)
    args.txpower = DEF_TXPOWER

output = [
    'systemnoise',
    'bandwidth',
    'distance',
    'frequency',
    'pathloss',
    'txgain',
    'txpower',
    'rxgain'
    # rxpower
    # rxsensitivity
    # sinr
]
metrics = {}
if args.verbose:
    for key in output:
        metrics[key] = getattr(args, key)

# https://github.com/adjacentlink/emane/wiki/Physical-Layer-Model#receive-power-calculation
if args.rxpower:
    metrics['txpower']   = txpower(args.rxpower, args.txgain, args.rxgain, args.pathloss)
    metrics['rxpower']   = args.rxpower
else:
    metrics['txpower']   = args.txpower
    metrics['rxpower']   = rxpower(args.txpower, args.txgain, args.rxgain, args.pathloss)

metrics['rxsensitivity'] = rxsensitivity(args.systemnoise, args.bandwidth)
metrics['sinr']          = sinr(metrics['rxpower'], metrics['rxsensitivity'])

# output
kwargs = {}
if args.prettyprint:
    kwargs['indent'] = args.prettyprint
print(json.dumps(metrics, **kwargs))
