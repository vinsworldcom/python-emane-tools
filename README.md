# Python Tools for EMANE

Python tools for interacting with EMANE.

## Files

This package provides Python modules installed to the default Python
package library installation location.  They can be `import` into custom
developed scripts.

Module | Description
----   | ----
Antenna/Event.py | Antenna module for pointing directional antennas with EMANE events.  This is a subclass of Coords.Sphere which is the azimuth and elevation angles to point.
ConfigParse.py   | A module to uniformly process the JSON config file.
Coords.py        | Routines ported from [EMANE](https://github.com/adjacentlink/emane/wiki/Computing-Antenna-Gain) for calculating and converting coordinate space.
NEM.py           | A NEM object that has the NEM number and its polar coordinates.

This package also provides Python command line scripts for some tasks.
They are installed to the default executable location.

File  | Description
----  | ----
eventAnt.py     | Script gets GPS positions from EMANE shell and points antennas according to input JSON file defining links.
trx.py          | Script using EMANE algorithms to calculate receive parameters (e.g., Rx power, Rx sensitivity, SINR, etc.) given variable inputs.

Example configuration files are provided.

Config File                 | Description
----                        | ----
elaps.json.example          | JSON config input file for most of the utilities above (-C argument).


## Installation

```
python3 -mpip install [--prefix=/usr] .
```

## Example:

```
#!/usr/bin/env python3

import emanetools.Coords as Coords

node1 = Coords.Polar()
node1.latitude, node1.longitude, node1.altitude = \
  (40.0, -72.0, 3.0)

node2 = Coords.Polar()
node2.latitude, node2.longitude, node2.altitude = \
  (40.0, -73.0, 3000.0)

forward = Coords.Sphere()
reverse = Coords.Sphere()
forward.calculate(node1, node2)
reverse.calculate(node2, node1)
print (forward.azimuth, forward.elevation, forward.distance)
print (reverse.azimuth, reverse.elevation, reverse.distance)
```

---

Copyright (c) - 2024 The MITRE Corporation. All Rights Reserved.

Public release case number:  24-1190
